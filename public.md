---
layout: page
title: Research
permalink: /public/
---
> This page includes my current public research, presentations and projects developed in collaboration with great researchers and academics. If you see something and want to check, please reach out.

### Papers
- Williams, T. (December, 2019) *An Approach Towards An Autonomic Computing Prototype Reference Architecture* [(master's thesis)](/papers/msthesis.pdf)

### Presentations
- "A Hackers Guide To Cheap NGFW" [Blacks in Cyberconf](https://www.blacksincyberconf.com/), October 3rd, 2020. [(conference presentation)](/slides/GuideToNGFW.pdf)
- "Defense Automation: SaltStack in a Buzzword Rich Environment" [SaltConf19](https://saltconf.com/), November 18th - 19th, 2019. [(conference presentation)](/slides/SaltConf19.pdf)
- "The not so Ominous Future of Computer System Defense" [Bsides Charlotte](https://www.bsidesclt.org), September 28th, 2019. [(conference presentation)](/slides/ThenotsoOminousFutureofComputerSystemDefense.pdf)
- "Hands-on Learning Experiences for Cyber Threat Hunting Education" [CAE in Cybersecurity Symposium](https://www.caecommunity.org/content/symposium-presentation-archive#), 2018. [(conference presentation)](https://webpages.uncc.edu/jwei8/Jinpeng_Homepage_files/CAE-2018-talk.pdf)

### Posters
- "Distributing the Network Security Barrier" [UNC Charlotte 2022 Graduate Research Symposium](https://sites.google.com/uncc.edu/grs2022/), March 25th, 2022, [Poster (Coming Soon)]()