---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: page
title: Service 
permalink: /service/
---
### Events
> [CarolinaCon Online 2](https://carolinacon.org/)

> [CarolinaCon Online](https://carolinacon.org/)
- Co-Organizer
- [CTF CC Online](https://github.com/49thSecurityDivision/CC-CTF-Online) Infrastructure Architect
- [CTF CC Online 2](https://github.com/49thSecurityDivision/CC-CTF-Online-2) Infrastructure Architect

> [BIC Secret Con](https://www.blacksincyberconf.com/events-1) -- CTF Challenge Contributor

### 2022 - 2023 (School Semester)
> [49th Security Division](https://49sd.com) -- Ethical Hacking Club
- Officer

### 2021 - 2022 (School Semester)
> [49th Security Division](https://49sd.com) -- Ethical Hacking Club
- Officer

### 2020 - 2021 (School Semester)
> [49th Security Division](https://49sd.com) -- Ethical Hacking Club
- Vice President
- Lab Manager
