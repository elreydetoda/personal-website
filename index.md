---
layout: page
title: Home
permalink: /
---

![Headshot](/assets/ava.jpg){: height="auto" width="150px" margin="auto" text-align="center"}

## About Me
My name is Trevon and I am a current PhD student and GAANN Fellow at UNC Charlotte where I am conducting research and receiving teaching mentorship from Professor Tom Moyer. I am studying applications and implementations of software defined networks, distributed systems and configuration management tools to develop more secure computing environments.

## Research Focus
- Distributed Systems
- Software Defined Networking
- Linux Containers

#### Additional Interestings
- Malware

#### Find me on the Internet
- [Github](https://github.com/trvon)
- [Gitlab](https://gitlab.com.com/trvon)
- Email me[@]trevon.dev
