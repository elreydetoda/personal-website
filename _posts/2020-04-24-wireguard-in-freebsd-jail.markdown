---
layout: post
title:  "Wireguard in a FreeBSD Jail"
date:   2020-04-24 13:00:00 +0000
categories: infrastructure
---

I spent the better part of my Friday night reinstalling my NAS server for homelab purposes. Through this experience, I found I wanted to capture what I learned and where I found answers for archiving purposes. I found many answers online that either didn't work for me or called for a different type of implementation. I link some notable guides and threads in the reference section below.

# Setup
Wireguard is pretty simple and straight forward to install on any unix system. If you have any questions or see a place were an update should be made, send me and email or contact me on keybase.

> - Create a jail
> Create jail conf. More information on creating and controlling jails can be found [here](https://www.freebsd.org/doc/handbook/jails-build.html)

		wireguard_jail {
		    vnet;
		    devfs_ruleset = "10";
		}

> - Install Wireguard in jail

		pkg install wireguard

> - Generate Keys

		wg genkey | tee privatekey | wg pubkey > publickey

> - Update rc.conf

		# Enable Wireguard
		wireguard_enable="YES"
		wireguard_interfaces="wg0"
		
		#Enable FireWall
		firewall_enable="YES"
		firewall_type="open"
		
		gateway_enable="YES"
		natd_enable="YES"
		natd_interface="[INTERFACE NAME]"
		natd_flags="-dynamic -m"

> - Create wireguard server conf 
> I created my file in /usr/local/etc/wireguard

		# wg0.conf
		[Interface]
		PrivateKey = [SERVER PRIVATE KEY]
		MTU = 1500
		Address = 172.16.1.1/24
		ListenPort = 51820
		
		[Peer]
		PublicKey = [CLIENT PUBLIC KEY]
		AllowedIPs = 172.16.1.5/32

> - Start wireguard

		service wireguard start


# References
- [An Introduction to Jails and Jail Networking](https://www.skyforge.at/posts/an-introduction-to-jails-and-jail-networking/)
- [FreeBSD Handbook: Firewall PF](https://www.freebsd.org/doc/handbook/firewalls-pf.html)
- [FreeBSD: Nat in PF](https://forums.freebsd.org/threads/nat-in-pf.22590/) 
- [WireGuard on FreeBSD Quick Look](https://genneko.github.io/playing-with-bsd/networking/freebsd-wireguard-quicklook/)
- [FreeNAS: Wireguard in Jail](https://www.ixsystems.com/community/threads/wireguard-on-base-system-forward-all-internet.82102/)
- [Unofficial Wireguard docs](https://github.com/pirate/wireguard-docs)
