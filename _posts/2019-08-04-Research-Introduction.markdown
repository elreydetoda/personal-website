---
layout: post
title:  "Research Introduction"
date:   2019-08-04 14:10:00 +0000
---

> #  Introduction
> *This blog excerpt serves as a high level overview of my research and current engagements and does not attempt to introduce my current lab work or dive into my findings. I plan to introduce my lab work and findings in depth in the coming months prior to the deadline for my thesis*  

> # Abstract

> The goal of the body of research I am engaging in, is to combine [Software Defined Network](https://en.wikipedia.org/wiki/Software-defined_networking) (SDN) technologies, [Network Function Virtualization](https://en.wikipedia.org/wiki/Network_function_virtualization) techniques, and Linux containers (LXD) to create a programmable environment that introduces a higher a level of [autonomic features](https://en.wikipedia.org/wiki/Autonomic_computing). This proposed environment will  address automated configuration and response in a plethora of environment/infrastructure OSI levels. Given the current centralized architecture of the [software defined network switch](https://www.openvswitch.org) and [controller](https://github.com/noxrepo/pox) integration, I am utilizing SaltStack to create an event driven API to handle the container and network configurations when triggered by generically defined events.  


> # Current State

> One of the most exciting features I have found during research is how the decision, load balance, and connection decision mechanism are implemented in [1] HoneyMix. Dicussed features facilitates attackers connecting to honeypots in the [honeynet environment](http://www.honeynet.org). In this implementation, the researchers are algorithmically determining the weight of a connection to a honeypot while utilizing the SDN controller to maintain the connection of an attacker to multiple honeypot end points. The idea of utilizing SDN to maintain a one to many connection from an attacker to a honeypot, is a pretty novel idea and has many implications in defense techniques. This led me to seek utilizing such a technique to maintain connections to multiple available containers to then isolate an attacker if they are determined by an algorithm to be malicious. The hope is to create a function that isolates an attacker in a post exploitation state, while still providing an active service if the determination is a false positive. This can be done by isolating the container from the rest of the environment after the API updates the OpenFlow rules.  

> Implementing moving target defense functionality while maintianing the integrity of connections is another interesting application of current on going work which I hope to implement in my research. I recently came across this idea in a paper on SDN-based solutions for Moving Target Defense [2], that discusses techniques that are potentially able to mitigate malicious traffic algorithmically. I won't dive too deeply into summarizing the paper or how I seek to integrate it, I only mention it to document it here.

> I hope to post my framework for starting, configuring and launching my test infrastructure, PySL, in the coming weeks. This should include the structure of the features defined in this post while being a testing bed for decision making functionality built into the API managing the network.


> # Technologies
> I am currently using the following technologies. Most have already been previously linked and referenced.

> - [POX Controller](https://github.com/noxrepo/pox)
- [OpenVSwitch](https://www.openvswitch.org)
- [PyPy](https://www.pypy.org)
- [SaltStack](https://www.saltstack.com)
- [LXD](https://linuxcontainers.org)
- [Docker/Kubernetes](https://www.docker.com/products/kubernetes)

> *Docker is specifically used to show that it can be ran in LXD containers*

> # References
> [1] Han, W., Zhao, Z., Doupé, A., & Ahn, G. (n.d.). HoneyMix : [Toward SDN-based Intelligent Honeynet](https://dl.acm.org/citation.cfm?id=2876022). 1–6.

> [2] Kampanakis, P., Perros, H., & Beyene, T. (2014). [SDN-based solutions for Moving Target Defense network protection](https://doi.org/10.1109/WoWMoM.2014.6918979). Proceeding of IEEE International Symposium on a World of Wireless, Mobile and Mult1imedia Networks 2014, WoWMoM 2014, 1–6.
